@extends ('master')

@section ('content')
<!--Start rev slider wrapper-->     
<section class="rev_slider_wrapper">
    <div id="slider1" class="rev_slider"  data-version="5.0">
        <ul>
           <li data-transition="slidingoverlayleft">
                <img src="images/slides/1.jpg"  alt="" width="1920" height="800" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="205" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-start="700">
                    <div class="slide-content-box">
                        <h1>Give Life For<br>Your Dream <span>Home</span></h1>
                        <h3>Why Over <span>1,000,000+</span> Customers Have Choosen Interior</h3>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="475" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn bg-cl-1" href="">Our Services</a>     
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="199" 
                    data-y="top" data-voffset="475" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn bdr" href="">Latest Projects</a>     
                        </div>
                    </div>
                </div>
            </li>
            <li data-transition="slidingoverlayleft">
                <img src="images/slides/2.jpg"  alt="" width="1920" height="800" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption tp-resizeme"
                    data-x="right" data-hoffset="0" 
                    data-y="top" data-voffset="205" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"  
                    data-start="500">
                    <div class="slide-content-box">
                        <h1>Being the best<br>decorators</h1>
                        <h3>You can work with us from anywhere of the <span>united states</span></h3>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="right" data-hoffset="209" 
                    data-y="top" data-voffset="475" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn bdr" href="">Latest Projects</a>     
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="right" data-hoffset="436" 
                    data-y="top" data-voffset="475" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn bg-cl-1" href="">Our Services</a>     
                        </div>
                    </div>
                </div>    
            </li>
            <li data-transition="slidingoverlayleft">
                <img src="images/slides/1.jpg"  alt="" width="1920" height="800" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="205" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-start="700">
                    <div class="slide-content-box">
                        <h1>We Won Best<br>Designer <span>Awards</span></h1>
                        <h3>Why Over <span>1,000,000+</span> Customers Have Choosen Interior</h3>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="475" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn bg-cl-1" href="">Our Services</a>     
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="199" 
                    data-y="top" data-voffset="475" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn bdr" href="">Latest Projects</a>     
                        </div>
                    </div>
                </div>
            </li>
            
        </ul>
    </div>
</section>
<!--End rev slider wrapper--> 


<!--Start welcome area-->
<section class="welcome-area">
    <div class="container clearfix">
        <div class="sec-title">
            <h2>Welcome to <span>Interior</span></h2>
            <span class="decor"></span>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="text-holder">
                    <h3>We are the best Interior designer since 1975.</h3>
                    <p>Interior brings 41 years of interior designs experience right to your home or office. Our design professionals are equipped to help you determine the products and design that work best for our customers within the colors and lighting of your surroundings more than your expectation.</p>
                    <p>Since our meetings take place in your home or office, we’ll work with you to help visualize a design solution that aligns with your taste, space, and budget, Also our team will guide you.</p>
                    <div class="bottom">
                       <div class="button">
                           <a class="thm-btn bg-cl-1" href="#">More About Us</a>
                       </div>
                       <div class="title">
                           <h3>Request Quote: <span>freequote@gmail.com</span></h3>
                       </div>     
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="gallery clearfix">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="video-gallery">
                                <img src="images/welcome/main-gallery.jpg" alt="Awesome Video Gallery">
                                <div class="overlay-gallery">
                                    <div class="icon-holder">
                                        <div class="icon">
                                            <a class="video-fancybox" title="Interrio Gallery" href="https://www.youtube.com/watch?v=KssOT2QVg-M"><img src="images/icon/play-btn.png" alt="Play Button"/></a>   
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="gallery-bg-img">
                                <img src="images/welcome/1.jpg" alt="Awesome Image">
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            
        </div>
    </div>
</section>
<!--End welcome area-->

<!--Start service area-->
<section class="service-area" style="background-image:url(images/services/bg.jpg);">
    <div class="container">
        <div class="sec-title">
            <h2>Services We do</h2>
            <span class="decor"></span>
        </div>
        <div class="row">
            <!--Start single service item-->
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="single-service-item">
                    <div class="img-holder">
                        <img src="images/services/1.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="services-single.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <a href="services-single.html"><h3>Residential <span>Design</span></h3></a>
                        <p>We design Wardrobes, Kitchen Cabinets based...</p>
                    </div>    
                </div>
            </div>
            <!--End single service item-->
            <!--Start single service item-->
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="single-service-item">
                    <div class="img-holder">
                        <img src="images/services/2.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="services-single.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <a href="services-single.html"><h3>Commercial <span>Design</span></h3></a>
                        <p>Best projects and products in the commercial...</p>
                    </div>    
                </div>
            </div>
            <!--End single service item-->
            <!--Start single service item-->
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="single-service-item">
                    <div class="img-holder">
                        <img src="images/services/3.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="services-single.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <a href="services-single.html"><h3>Corporate <span>Design</span></h3></a>
                        <p>Changing business climate, workplace design...</p>
                    </div>    
                </div>
            </div>
            <!--End single service item-->
            <!--Start single service item-->
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="single-service-item">
                    <div class="img-holder">
                        <img src="images/services/4.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="services-single.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <a href="services-single.html"><h3>Hospitality <span>Design</span></h3></a>
                        <p>Designing hospital area with equipments needs...</p>
                    </div>    
                </div>
            </div>
            <!--End single service item-->
            <!--Start single service item-->
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="single-service-item">
                    <div class="img-holder">
                        <img src="images/services/5.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="services-single.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <a href="services-single.html"><h3>Restaurant <span>Design</span></h3></a>
                        <p>Will differentiate from the ordinary restaurants...</p>
                    </div>    
                </div>
            </div>
            <!--End single service item-->
            <!--Start single service item-->
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="single-service-item">
                    <div class="img-holder">
                        <img src="images/services/6.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="services-single.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <a href="services-single.html"><h3>Industrial <span>Design</span></h3></a>
                        <p>Will differentiate you from the ordinary restaurants</p>
                    </div>    
                </div>
            </div>
            <!--End single service item-->
            
        </div>
    </div>
</section>
<!--End service area-->

<!--Start latest project area-->
<section class="latest-project-area">
    <div class="container-fluid">
        <div class="container">
            <div class="sec-title pull-left">
                <h2>Latest <span>Projects</span></h2>
                <span class="decor"></span>
            </div>   
            <ul class="project-filter post-filter pull-right">
                <li class="active" data-filter=".filter-item"><span>View All (12)</span></li>
                <li data-filter=".residential"><span>Residential (04)</span></li>
                <li data-filter=".corporate"><span>Corporate (03)</span></li>
                <li data-filter=".restaurant"><span>Restaurant (02)</span></li>
                <li data-filter=".industrial"><span>Industrial (03)</span></li>
            </ul>
        </div>
        <div class="row project-content masonary-layout filter-layout">
            <!--Start single project-->
            <div class="col-md-3 col-sm-6 col-xs-12 filter-item corporate residential industrial">
                <div class="single-project-item">
                    <div class="img-holder">
                        <img src="images/project/lat-pj-1.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="project-single-v1.html"><h3>Modern Living Room</h3></a>
                                    <p>Residential</p>
                                    <div class="icon-holder">
                                        <a href="images/project/lat-pj-1.jpg" data-rel="prettyPhoto" title="Interrio Project"><i class="fa fa-camera"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
            <!--End single project-->
            <!--Start single project-->
            <div class="col-md-3 col-sm-6 col-xs-12 filter-item industrial corporate">
                <div class="single-project-item">
                    <div class="img-holder">
                        <img src="images/project/lat-pj-2.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="project-single-v1.html"><h3>Modern Living Room</h3></a>
                                    <p>Residential</p>
                                    <div class="icon-holder">
                                        <a href="images/project/lat-pj-2.jpg" data-rel="prettyPhoto" title="Interrio Project"><i class="fa fa-camera"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
            <!--End single project-->
            <!--Start single project-->
            <div class="col-md-3 col-sm-6 col-xs-12 filter-item corporate residential restaurant">
                <div class="single-project-item">
                    <div class="img-holder">
                        <img src="images/project/lat-pj-3.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="project-single-v1.html"><h3>Modern Living Room</h3></a>
                                    <p>Residential</p>
                                    <div class="icon-holder">
                                        <a href="images/project/lat-pj-3.jpg" data-rel="prettyPhoto" title="Interrio Project"><i class="fa fa-camera"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
            <!--End single project-->
            <!--Start single project-->
            <div class="col-md-3 col-sm-6 col-xs-12 filter-item corporate residential industrial">
                <div class="single-project-item">
                    <div class="img-holder">
                        <img src="images/project/lat-pj-4.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="project-single-v1.html"><h3>Modern Living Room</h3></a>
                                    <p>Residential</p>
                                    <div class="icon-holder">
                                        <a href="images/project/lat-pj-4.jpg" data-rel="prettyPhoto" title="Interrio Project"><i class="fa fa-camera"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
            <!--End single project-->
            <!--Start single project-->
            <div class="col-md-3 col-sm-6 col-xs-12 filter-item residential corporate industrial">
                <div class="single-project-item">
                    <div class="img-holder">
                        <img src="images/project/lat-pj-5.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="project-single-v1.html"><h3>Modern Living Room</h3></a>
                                    <p>Residential</p>
                                    <div class="icon-holder">
                                        <a href="images/project/lat-pj-5.jpg" data-rel="prettyPhoto" title="Interrio Project"><i class="fa fa-camera"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
            <!--End single project-->
            <!--Start single project-->
            <div class="col-md-3 col-sm-6 col-xs-12 filter-item residential industrial restaurant">
                <div class="single-project-item">
                    <div class="img-holder">
                        <img src="images/project/lat-pj-6.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="project-single-v1.html"><h3>Modern Living Room</h3></a>
                                    <p>Residential</p>
                                    <div class="icon-holder">
                                        <a href="images/project/lat-pj-6.jpg" data-rel="prettyPhoto" title="Interrio Project"><i class="fa fa-camera"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
            <!--End single project-->
            <!--Start single project-->
            <div class="col-md-3 col-sm-6 col-xs-12 filter-item residential corporate restaurant">
                <div class="single-project-item">
                    <div class="img-holder">
                        <img src="images/project/lat-pj-7.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="project-single-v1.html"><h3>Modern Living Room</h3></a>
                                    <p>Residential</p>
                                    <div class="icon-holder">
                                        <a href="images/project/lat-pj-7.jpg" data-rel="prettyPhoto" title="Interrio Project"><i class="fa fa-camera"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
            <!--End single project-->
            <!--Start single project-->
            <div class="col-md-3 col-sm-6 col-xs-12 filter-item residential industrial">
                <div class="single-project-item">
                    <div class="img-holder">
                        <img src="images/project/lat-pj-8.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="project-single-v1.html"><h3>Modern Living Room</h3></a>
                                    <p>Residential</p>
                                    <div class="icon-holder">
                                        <a href="images/project/lat-pj-8.jpg" data-rel="prettyPhoto" title="Interrio Project"><i class="fa fa-camera"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
            <!--End single project-->
        </div>     
    </div>
</section>
<!--End latest project area-->

<!--Start slogan area-->
<section class="slogan-area" style="background-image:url(images/resources/slogan.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="slogan">
                    <h2>Being the largest in-home decorationg service in newyork city, you can work with us from anywhere!</h2>
                </div>
            </div>
        </div>     
    </div>
</section>
<!--End slogan area-->

<!--Start working area-->
<section class="working-area">
    <div class="container">
        <div class="sec-title text-center">
            <h2>Our Working Process</h2>
            <p>We've distilled our interior design process into 4 Steps – the same steps we have been using for more than 41 years, In this steps, the designer visits your home to gather more.</p>
        </div>  
        <div class="row">
            <!--Start single working item-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-working-item">
                   <div class="icon-box">
                        <div class="icon">
                            <span class="flaticon-people"></span>
                        </div>
                        <div class="count">
                            <h3>1</h3>
                        </div>    
                   </div>
                   <div class="text-box text-center">
                       <h3>Meet Customers</h3>
                       <p>Our designers will meet the customer to gather more Information.</p>
                   </div>     
                </div>
            </div>
            <!--End single working item-->
            <!--Start single working item-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-working-item">
                   <div class="icon-box">
                        <div class="icon">
                            <span class="flaticon-people"></span>
                        </div>
                        <div class="count">
                            <h3>2</h3>
                        </div>    
                   </div>
                   <div class="text-box text-center">
                       <h3>Meet Customers</h3>
                       <p>Our designers will meet the customer to gather more Information.</p>
                   </div>     
                </div>
            </div>
            <!--End single working item-->
            <!--Start single working item-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-working-item">
                   <div class="icon-box">
                        <div class="icon">
                            <span class="flaticon-people"></span>
                        </div>
                        <div class="count">
                            <h3>3</h3>
                        </div>    
                   </div>
                   <div class="text-box text-center">
                       <h3>Meet Customers</h3>
                       <p>Our designers will meet the customer to gather more Information.</p>
                   </div>     
                </div>
            </div>
            <!--End single working item-->
            <!--Start single working item-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-working-item">
                   <div class="icon-box">
                        <div class="icon">
                            <span class="flaticon-people"></span>
                        </div>
                        <div class="count">
                            <h3>4</h3>
                        </div>    
                   </div>
                   <div class="text-box text-center">
                       <h3>Meet Customers</h3>
                       <p>Our designers will meet the customer to gather more Information.</p>
                   </div>     
                </div>
            </div>
            <!--End single working item-->
        </div>
    </div>
</section>
<!--End working area-->

<!--Start testimonial area-->
<section class="testimonial-area">
    <div class="container">
        <div class="sec-title text-center">
            <h2>Customers Feedback</h2>
            <span class="border"></span>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="testimonials-carousel">
                    <!--Start single slide item-->
                    <div class="single-slide-item">
                        <div class="img-box">
                            <img src="images/customers/gallery.jpg" alt="Awesome Image">
                            <div class="client-photo">
                                <img src="images/customers/customer-1.png" alt="Awesome Image">
                            </div>
                            <div class="review-box">
                                <span>Project Rating:</span>
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="text-box">
                            <span class="flaticon-right"></span>
                            <div class="text">
                                <p>Today we can tell you, thanks to your passion, hard work creativity, and expertise, you delivered us the most beautiful house ever! It’s been a beautiful ride, there were up’s and down’s, frustrations, delays at the same time great looks.</p>
                                <h3>Astley Fletcher</h3>
                                <h4>Newyork City</h4>
                            </div>
                        </div>
                    </div>
                    <!--End single slide item-->
                    <!--Start single slide item-->
                    <div class="single-slide-item">
                        <div class="img-box">
                            <img src="images/customers/gallery.jpg" alt="Awesome Image">
                            <div class="client-photo">
                                <img src="images/customers/customer-1.png" alt="Awesome Image">
                            </div>
                            <div class="review-box">
                                <span>Project Rating:</span>
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="text-box">
                            <span class="flaticon-right"></span>
                            <div class="text">
                                <p>Today we can tell you, thanks to your passion, hard work creativity, and expertise, you delivered us the most beautiful house ever! It’s been a beautiful ride, there were up’s and down’s, frustrations, delays at the same time great looks.</p>
                                <h3>Astley Fletcher</h3>
                                <h4>Newyork City</h4>
                            </div>
                        </div>
                    </div>
                    <!--End single slide item-->
                    <!--Start single slide item-->
                    <div class="single-slide-item">
                        <div class="img-box">
                            <img src="images/customers/gallery.jpg" alt="Awesome Image">
                            <div class="client-photo">
                                <img src="images/customers/customer-1.png" alt="Awesome Image">
                            </div>
                            <div class="review-box">
                                <span>Project Rating:</span>
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="text-box">
                            <span class="flaticon-right"></span>
                            <div class="text">
                                <p>Today we can tell you, thanks to your passion, hard work creativity, and expertise, you delivered us the most beautiful house ever! It’s been a beautiful ride, there were up’s and down’s, frustrations, delays at the same time great looks.</p>
                                <h3>Astley Fletcher</h3>
                                <h4>Newyork City</h4>
                            </div>
                        </div>
                    </div>
                    <!--End single slide item-->
                </div>
            </div>
        </div>
    </div>
</section>
<!--End testimonial area-->

<!--Start latest blog area-->
<section class="latest-blog-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sec-title pull-left">
                    <h2>Latest <span>News</span></h2>
                    <span class="decor"></span>
                </div>
                <div class="more-blog-button pull-right">
                    <a class="thm-btn bg-cl-1" href="news-standard.html">More News</a> 
                </div>
            </div>
        </div>   
        <div class="row">
            <!--Start single blog item-->
            <div class="col-md-4">
                <div class="single-blog-item">
                    <div class="img-holder">
                        <img src="images/blog/lat-blog-1.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-single.html"><i class="fa fa-link" aria-hidden="true"></i></a> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <a href="blog-single.html">
                            <h3 class="blog-title">Stylish Ways to Display Your TV</h3>
                        </a>
                        <ul class="meta-info">
                            <li><i class="fa fa-user" aria-hidden="true"></i><a href="#">Fletcher</a></li>
                            <li><i class="fa fa-clock-o" aria-hidden="true"></i><a href="#">April 21, 2016</a></li>
                        </ul>
                        <div class="text">
                            <p>How all this mistaken idea of denouncing pleasure and praising uts pain was borns and give you How all this mistaken.</p>
                            <a class="readmore" href="news-single.html">Read More<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    </div>    
                </div>    
            </div>
            <!--End single blog item-->
            <!--Start single blog item-->
            <div class="col-md-4">
                <div class="single-blog-item">
                    <div class="img-holder">
                        <img src="images/blog/lat-blog-2.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-single.html"><i class="fa fa-link" aria-hidden="true"></i></a> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <a href="blog-single.html">
                            <h3 class="blog-title">Get Pleasant Dining Experience</h3>
                        </a>
                        <ul class="meta-info">
                            <li><i class="fa fa-user" aria-hidden="true"></i><a href="#">Venanda</a></li>
                            <li><i class="fa fa-clock-o" aria-hidden="true"></i><a href="#">March 4, 2016</a></li>
                        </ul>
                        <div class="text">
                            <p>Master-builder of human happiness no one rejects, dislikes, or avoids ut itself, because it is pleasures.</p>
                            <a class="readmore" href="news-single.html">Read More<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    </div>    
                </div>    
            </div>
            <!--End single blog item-->
            <!--Start single blog item-->
            <div class="col-md-4">
                <div class="single-blog-item">
                    <div class="img-holder">
                        <img src="images/blog/lat-blog-3.jpg" alt="Awesome Image">
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-single.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-holder">
                        <a href="blog-single.html">
                            <h3 class="blog-title">Smart Ways to Store Appliances</h3>
                        </a>
                        <ul class="meta-info">
                            <li><i class="fa fa-user" aria-hidden="true"></i><a href="#">Bovando</a></li>
                            <li><i class="fa fa-clock-o" aria-hidden="true"></i><a href="#">Jan 25, 2016</a></li>
                        </ul>
                        <div class="text">
                            <p>Great explorer of the truth, the master seds builder of human happiness. No one rejects, dislikes, rationally encounter.</p>
                            <a class="readmore" href="news-single.html">Read More<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    </div>    
                </div>    
            </div>
            <!--End single blog item-->
        </div>
    </div>
</section>
<!--End latest blog area-->
 
<!--Start Brand area-->  
<section class="brand-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="brand">
                    <!--Start single item-->
                    <div class="single-item">
                        <a href="#"><img src="images/brand/1.png" alt="Awesome Brand Image"></a>
                    </div>
                    <!--End single item-->
                    <!--Start single item-->
                    <div class="single-item">
                        <a href="#"><img src="images/brand/2.png" alt="Awesome Brand Image"></a>
                    </div>
                    <!--End single item-->
                    <!--Start single item-->
                    <div class="single-item">
                        <a href="#"><img src="images/brand/3.png" alt="Awesome Brand Image"></a>
                    </div>
                    <!--End single item-->
                    <!--Start single item-->
                    <div class="single-item">
                        <a href="#"><img src="images/brand/4.png" alt="Awesome Brand Image"></a>
                    </div>
                    <!--End single item-->
                    <!--Start single item-->
                    <div class="single-item">
                        <a href="#"><img src="images/brand/5.png" alt="Awesome Brand Image"></a>
                    </div>
                    <!--End single item-->
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Brand area-->       
  



@endsection